# Summary

Date : 2019-12-30 11:22:42

Directory h:\Nodejs\school\routes

Total : 12 files,  613 codes, 18 comments, 76 blanks, all 707 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 12 | 613 | 18 | 76 | 707 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 12 | 613 | 18 | 76 | 707 |

[details](details.md)