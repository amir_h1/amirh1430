const Express = require('express');
const Morgan = require('morgan');
const BodyParser = require('body-parser');
const RegisterRoute = require('./routes/registerUser');
const LoginRoute = require('./routes/loginUser');
const AddUser = require('./routes/addUsers');
const student = require('./routes/student');
const course = require('./routes/course');
const Classes = require('./routes/classes');
const personnel = require('./routes/personnel');
const classList = require('./routes/classList');
const abpr = require('./routes/abPr');
const message = require('./routes/message');
const behavior = require('./routes/behavior');
const show = require('./routes/show');
const score = require('./routes/score');
const app = Express();

app.use(Morgan('dev'));
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());

app.use('/register', RegisterRoute);
app.use('/login', LoginRoute);
app.use('/addUser', AddUser);
app.use('/student', student);
app.use('/course', course);
app.use('/score', score);
app.use('/abpr', abpr);
app.use('/behavior', behavior);
app.use('/show', show);
app.use('/message', message);
app.use('/personnel', personnel);
app.use('/classlist', classList);
app.use('/classes', Classes);

app.use((req, res, next) => {
    res.status(404).json({
        result: `${res.statusCode} not found`
    });
});

module.exports = app;