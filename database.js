const Mongodb = require('mongodb').MongoClient;
const Assert = require('assert');

const url = 'mongodb://admin:admin@198.143.183.248/';

const client = new Mongodb(url, { useUnifiedTopology: true });
client.connect((err) => {
    Assert.equal(null, err);
    console.log('connected to mongoDB');
});
module.exports = client;
