const Express = require('express');
const app = Express();
const database = require('../database');
const dbabpr = database.db('school').collection('abpr');
const dbclass = database.db('school').collection('classes');
const dbstudents = database.db('school').collection('unRegisterUsers');
const oj = require('mongodb').ObjectID;

app.get('/', async (req, res, next) => {
    let listClasses = [];
    await dbclass.find({}).forEach((f) => {
        listClasses.push(f.classID);
    });
    res.status(200).json({ result: listClasses });
});


app.post('/', async (req, res, next) => {
    let listStudents = [];
    const classID = { class: req.body.classID };
    const find = dbstudents.find(classID);
    await find.forEach((f) => {
        listStudents.push(f);
    });
    if (listStudents.length == 0) return res.status(404).json({ result: 'not found' });

    res.status(200).json({ result: listStudents });
})
app.post('/send', async (req, res, next) => {
    const classID = req.body.classID;
    const teacherID = req.body.teacherID;
    const time = req.body.time;
    const abpr = req.body.abpr;
    await dbabpr.insertOne({
        time: time,
        classID: classID,
        teacherID: teacherID,
        abpr: abpr,
    });
    var list = [];
    await dbstudents.find({ class: classID }).forEach((f) => {
        list.push(f);
    })
    for (const key in list) {
        const fi = list[key].abpr;
        fi.push(abpr[key]);
        await dbstudents.updateOne({ '_id': oj(list[key]._id) }, { $set: { abpr: fi } })
    }
    res.status(200).json({ result: 'sended' })
})
app.get('/absent', async (req, res, next) => {
    const list = [];
    await dbabpr.find({}).forEach((f) => {
        list.push(f);
    })
    res.status(200).json({ result: list })

})
module.exports = app;
