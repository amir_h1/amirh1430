const Express = require('express');
const database = require('../database').db('school');
const dbstudent = database.collection('unRegisterUsers');
const oj = require('mongodb').ObjectID;
const app = Express();

app.post('/', async (req, res) => {
    const id = { _id: oj(req.body.id) };
    const teacherId = req.body.teacherId;
    const time = req.body.time;
    const content = req.body.content;

    const find = await dbstudent.findOne(id)
    if (!find) return res.status(404).json({ result: 'not found' })
    let behavior = [];
    console.log(find);
    
    find.behavior.forEach((f) => {
        behavior.push(f);
    });

    behavior.push({
        from: teacherId,
        time: time,
        for: id._id,
        content: content,
    })

    await dbstudent.updateOne(id, { $set: { behavior: behavior } })
    res.status(200).json({ result: 'send' })
})

module.exports = app;