const Express = require('express');
const App = Express();
const database = require('../database');

const db = database.db('school').collection('regUsers');

App.post('/', async (req, res, next) => {
    const idNumber = { idNumber: req.body.idNumber }
    const password = { password: req.body.password }
    const find = await db.findOne(idNumber);
    if (!find) return res.status(404).json({ result: `${res.statusCode} not found` });
    if (find.password !== password.password) return res.status(401).json({ result: `${res.statusCode} icorrect password` })
    res.status(200).json({
        result: {
            _id: find._id,
            name: find.password,
            family: find.family,
            name: find.password,
            class: find.class,
            phonenumber: find.phonenumber,
        }
    })
});

module.exports = App;