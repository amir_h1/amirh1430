const Express = require('express');
const app = Express();
const db = require('../database').db('school');
const dbpersonnel = db.collection('presonnel');
const dbstudent = db.collection('unRegisterUsers');
const dbcourse = db.collection('courses');
const oj = require('mongodb').ObjectID;

const personnels = [
    {
        ID: 1000, name: 'amir', family: 'ahamadi', authLvl: 2, password: null, isRegister: false,
    }, {
        ID: 1001, name: 'akbar', family: 'irani', authLvl: 4, password: null, isRegister: false,
    }, {
        ID: 1002, name: 'bahram', family: 'mohamadi', authLvl: 5, password: null, isRegister: false,
    }, {
        ID: 1003, name: 'asqar', family: 'mohamadi', authLvl: 3, password: null, isRegister: false,
    }, {
        ID: 1004, name: 'armin', family: 'moshfeq', authLvl: 3, password: null, isRegister: false,
    }, {
        ID: 1005, name: 'ali', family: 'tehrani', authLvl: 3, password: null, isRegister: false,
    }, {
        ID: 1006, name: 'amir', family: 'akbari', authLvl: 3, password: null, isRegister: false,
    }, {
        ID: 1007, name: 'javad', family: 'mohamadi', authLvl: 3, password: null, isRegister: false,
    },
];

app.post('/', async (req, res, next) => {
    await dbpersonnel.insertMany(personnels);
    res.status(201).json({ result: 'added' })
})

app.get('/', async (req, res, next) => {
    var persons = []
    await dbpersonnel.find({ isRegister: true }).forEach((r) => {
        persons.push(r)
    });
    res.status(200).json({ result: persons })
})

app.put('/register', async (req, res, next) => {
    const user = { ID: req.body.id };
    const password = req.body.password;
    const find = await dbpersonnel.findOne(user);
    if (!find) return res.status(404).json({ result: 'invalid ID' });
    if (find.isRegister == true) return res.status(500).json({ result: 'you registerd' });
    await dbpersonnel.updateOne(find, { $set: { password: password, isRegister: true } });
    res.status(200).json({ result: find })

})
app.post('/login', async (req, res, next) => {
    const username = { ID: req.body.username }
    const password = { password: req.body.password }
    const find = await dbpersonnel.findOne(username);
    if (!find) return res.status(404).json({ result: 'invalid ID' });
    if (find.isRegister == false) return res.status(500).json({ result: 'not register' });
    if (find.password !== password.password) return res.status(500).json({ result: 'wrong password' });
    res.status(201).json({ result: find });
})
app.get('/unregister', async (req, res, next) => {
    var persons = []
    await dbpersonnel.find({ isRegister: false }).forEach((r) => {
        persons.push(r)
    });
    res.status(200).json({ result: persons })
})

app.get('/abpr', async (req, res) => {
    var courses = [];
    await dbcourse.find({}).forEach((f) => {
        const name = f.lacture;
        const teacherId = f.teacherID.ID;
        const teacherName = f.teacherID.name;
        const teacherFamily = f.teacherID.family;
        courses.push({ name, teacherId, teacherName, teacherFamily });
    });

    var students = []
    await dbstudent.find({}).forEach((f) => {
        students.push(f);
    })
    var abpr = []
    students.forEach((f) => {
        f.abpr.forEach((x) => {
            if (x.AB == false) {
                abpr.push(x);
            }
        })
    })

    abpr.forEach((f) => {
        students.filter((x) => {
            var id1 = oj(x._id).toString();
            var id2 = f.id;

            if (id1 === id2) {
                f.id = {
                    phonenumber: x.phonenumber,
                    class: x.class,
                    isRegister: x.isRegister,
                }
            }
        })
        courses.filter((x) => {
            if (x.teacherId === f.from) {
                f.from = x;
            }
        })
    })

    res.status(200).json({ result: abpr })
})

app.get('/students', async (req, res) => {
    var students = [];
    await dbstudent.find({}).forEach(f => {
        students.push(f);
    })
    res.status(200).json({ result: students })
})

module.exports = app;