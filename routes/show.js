const Express = require('express');
const database = require('../database').db('school');
const dbstudent = database.collection('unRegisterUsers');
const dbcourse = database.collection('courses');
const app = Express();

app.post('/', async (req, res, next) => {
    const classID = { class: req.body.classID };
    const teacherID = { teacherID: req.body.teacherID };
    if (!classID.class) return res.status(500).json({ result: 'class id miss' });
    if (!teacherID.teacherID) return res.status(500).json({ result: 'teacher id miss' });

    var list = [];
    await dbstudent.find(classID).forEach((r) => {
        list.push(r);
    });

    var courses = [];
    await dbcourse.find({}).forEach((f) => {
        const name = f.lacture;
        const teacherId = f.teacherID.ID;
        const teacherName = f.teacherID.name;
        const teacherFamily = f.teacherID.family;
        courses.push({ name, teacherId, teacherName, teacherFamily });
    });

    var scores = [];
    list.forEach((f, index) => {
        scores.push({
            id: list[index]._id,
            username: list[index].username,
            family: list[index].family,
            class: list[index].class,
            abpr: list[index].abpr,
            behavior: list[index].behavior,
            scores: list[index].scores,
        });
    })
    var scoreRemove = [];
    var abprRemove = [];
    var behaviorRemove = [];

    for (const item of list) {
        await item.scores.forEach(async (f, index) => {
            if (item.scores[index].from === teacherID.teacherID) {
                await courses.find((a) => {
                    if (a.teacherId == f.from) {
                        item.scores[index].from = a;
                    }
                })
            } else {
                scoreRemove.push(item.scores[index])
            }
        })

        await item.abpr.forEach(async (f, index) => {
            if (item.abpr[index].from === teacherID.teacherID) {
                await courses.find((a) => {
                    if (a.teacherId == f.from) {
                        item.abpr[index].from = a;
                    }
                })
            } else {
                abprRemove.push(item.abpr[index])
            }
        })
        await item.behavior.forEach(async (f, index) => {
            if (item.behavior[index].from === teacherID.teacherID) {
                await courses.find((a) => {
                    if (a.teacherId == f.from) {
                        item.behavior[index].from = a;
                    }
                })
            } else {
                behaviorRemove.push(item.behavior[index])
            }
        })
    }

    scores.forEach((f) => {
        for (const i of scoreRemove) {
            const x = f.scores.indexOf(i)
            f.scores.splice(x, 1)
        }
    })
    scores.forEach((f) => {
        for (const i of abprRemove) {
            const x = f.abpr.indexOf(i)
            f.abpr.splice(x, 1)
        }
    })
    scores.forEach((f) => {
        for (const i of behaviorRemove) {
            const x = f.behavior.indexOf(i)
            f.behavior.splice(x, 1)
        }
    })

    res.status(200).json({ result: scores });
});


module.exports = app;
