const Http = require('http');
const port = process.env.port || 3000;
const App = require('./app');
require('./database');
const Server = Http.createServer(App);

Server.listen(port, () => console.log(`Server running on port: ${port}`));